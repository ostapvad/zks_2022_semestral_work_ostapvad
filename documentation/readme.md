Autory: Artem Hurbych (hurbyart), Pavel Paklonski (paklopav).

Odkaz na Git: https://gitlab.fel.cvut.cz/hurbyart/omo-semestralka

Pozadavky:

F1. Mame 7 parties: Customer, Retailer, Seller, Storehouse, Manufacturer, Farmer (VegetableFarmer and MeatFarmer). Jsou v balicku chainParticipantsTests.

F2. Potraviny se predavaji pres MeatChannel, VegetableChannel a ReadyMealChannel. Penize se predava pres PaymentChannel. Jsou v balicku channels. Farmer pestuje ingedienty pro ready meals. Manufacturer dela ready meal z ingredientu. Storehouse uchovava potraviny podle teploty chraneni. Seller vykonova roli velkoobchodniho prodeje. Retailer je jako maly obchod. Customer muze jen nakupovat. Muze nakupovat jako ingredient, tak i readyMeal.

F3. Blockchain je realizovan pomoci Hashcodu. Kazda party ma list transakci. Kazda transakce ma svuj hashcode a hashcode predchozi transakce. Pomoci observeru se kazda udelana transakce pridava do listu kazde party. Kdyz se predava jeji previousHash srovanava se s hashem posledni transakce v listu kazdeho party. Kdyz previousHash bude jiny logger vypise warning

F4. FoodEntity ma metody writeAllActions a writeAllActionsText. Prvni vypise na stdout vsechny operace s ni, druha vygeneruje textak.

F5. Jsou 4 kanaly v balicku channels. Pres VegetableChannel, MeatChannel a ReadyMealChannel party predavaji potraviny. PaymentChannel je vyuzivan pro placeni.

F6. Kdyz se provede transakce jeji hash se porovna s hashem kazde transakce v listu transakci u kazde party. Kdyby ten hash byl stejny tak logger vypise warning a string s temi transakce prida do listu pro security report u transactionInformeru. Z toho listu pak je mozne vygenerovat report pomoci metod makeSecurityReport a makeTextSecurityReport.

F7. Logger vypisuje warning, pokud previuosHash neni roven hashu posledni transakce. Uz je pozepsano v F3.

F8. Potravina ma 3 stavy. Jsou v balicku states. Potravina muze byt vyuzivana jenom kdyz ma UnpackedState. Nemuze byt transportovana kdyz je rozbalena. Dojit ke Customeru muze jen kdyz ma FinalPackedState.

F9. Kazdy kanal ma zadatele a vykonavatele, pokud nekdo chce vyskat pozadavek on musi byt pripojen ke kanalu. Udela to pomoci metody makeRequest. Pak ta party, ktera muze vykonat ten request, prijima ten request, zadatel se pta kolik to bude stat a pak pokud ma dost penez, vykonavatel zacne zpracovavat potravinu. Po zpracovani vykonavatel dostava penize pres PaymentChannel. Konecne zadatel dostava potravinu.

F10. Parties Report: metody generatePartiesReport a generateTextPartiesReport v Mainu. Jsou udelany pomoci design patternu Iterator.
     Foodchain Report: F4.
     Transaction Report: metody makeReport a generateTextReport v TransactionInformer.


Design patterns (in English)

1. Observer
The pattern is used for notifying all participants about new transaction that take place during the execution of the program.

2. State
Is used for changing and controlling state of product (packed, temporary packed, finally packed).

3. Strategy
Is used for different receipts to cook dish from ingredients. (package strategy, classes CookBorshch, CookPancakes, etc)

4. Iterator
Is used to iterate all events in party report.

5. Singleton
Is used for creating only one MeatChannel, VegetableChannel, ReadyMealChannel and only one PaymentChannel.

6. Visitor
Is used for filling prices of each party.

Vlastní inovace je v tom, ze jsou ingredienty a z nich Manufacturer dela ReadyMealy.