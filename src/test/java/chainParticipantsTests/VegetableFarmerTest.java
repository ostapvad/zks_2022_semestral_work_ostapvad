package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.visitor.Visitor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.GROW;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class VegetableFarmerTest {
    VegetableFarmer vegetableFarmer;
    TransactionInformer transactionInformer;
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants;
    @BeforeEach
    public void init(){
        channelParticipants = new ArrayList<>();
        transactionInformer = new TransactionInformer();
        vegetableFarmer = new VegetableFarmer("Mykola", "Ukraine", 500, 1976,
                vegetableIngredients, transactionInformer);
    }
    /*
    * Ensures that Vegetable Farmer registers correctly to all channels
    * */
    @Test
    @Tag("UnitTest")
    public void VegetableFarmerRegisterToChannelCorrectlyTest(){
        // Arrange
        channelParticipants.add(vegetableFarmer);
        PaymentChannel.getPaymentChannel(channelParticipants, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        // Act
        vegetableFarmer.registerToTheChannel();
        // Assert
        Assertions.assertFalse(ReadyMealChannel.getReadyMealChannel().isRegister(vegetableFarmer));
        Assertions.assertFalse(MeatChannel.getMeatChannel().isRegister(vegetableFarmer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(vegetableFarmer));
        Assertions.assertTrue(VegetableChannel.getVegetableChannel().isRegister(vegetableFarmer));
    }


    /*
     * Tests that Vegetable Farmer processes Food correctly
     * */
    @Test
    @Tag("UnitTest")
    public void VegetableFarmerCorrectlyProcessFoodTest(){
        // Arrange
        Request request = new Request(vegetableFarmer, BEET, 1,  vegetableFarmer.getOperationType());
        // Act
        FoodEntity vegetableFarmerFood = vegetableFarmer.process(request);
        // Assert
        Assertions.assertEquals(BEET, vegetableFarmerFood.getName());
        Assertions.assertEquals(1, vegetableFarmerFood.getQuantity());
        Assertions.assertEquals(8, vegetableFarmerFood.getStorageTemperature());
    }
    /*
     * Tests that Vegetable Farmer is not able to process specific food
     * */
    @Test
    @Tag("UnitTest")
    public void VegetableFailsToProcessFoodTest(){
        // Arrange
        Request request = new Request(vegetableFarmer, MEAT, 1,  vegetableFarmer.getOperationType());
        // Act
        FoodEntity vegetableFarmerFood = vegetableFarmer.process(request);
        // Assert
        Assertions.assertNull(vegetableFarmerFood);
    }

    /*
     * Ensures that Vegetable Farmer operation is always GROW
     * */
    @Test
    @Tag("UnitTest")
    public void VegetableFarmerOperationIsAlwaysGrowTest(){
        // Act
        OperationEnum vegetableFarmerOperation = vegetableFarmer.getOperationType();
        // Assert
        Assertions.assertEquals(GROW, vegetableFarmerOperation);
    }

}



