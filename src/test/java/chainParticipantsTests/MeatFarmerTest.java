package chainParticipantsTests;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.GROW;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class MeatFarmerTest {
    MeatFarmer meatFarmer;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants;
    @BeforeEach
    public void init(){
        channelParticipants = new ArrayList<>();
        transactionInformer = new TransactionInformer();
        meatFarmer = new MeatFarmer("Bill", "USA",  1000,1991,
                meatIngredients, transactionInformer);
    }
    /*
    * Ensures that Meat Farmer registers only to 2 channels
    * */
    @Test
    @Tag("UnitTest")
    public void registerToTheChannelCorrectTest(){
        // Arrange
        channelParticipants.add(meatFarmer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Act
        meatFarmer.registerToTheChannel();
        // Assert
        Assertions.assertFalse(ReadyMealChannel.getReadyMealChannel().isRegister(meatFarmer));
        Assertions.assertFalse(VegetableChannel.getVegetableChannel().isRegister(meatFarmer));
        Assertions.assertTrue(MeatChannel.getMeatChannel().isRegister(meatFarmer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(meatFarmer));
    }
    /*
    * Tests that Meat Farmer processes Food Entity
    * */
    @Test
    @Tag("UnitTest")
    public void MeatFarmerCorrectlyProcessFoodTest(){
        // Arrange
        Request request = new Request(meatFarmer, BUTTER, 1,  meatFarmer.getOperationType());
        // Act
        FoodEntity meatFarmerFood = meatFarmer.process(request);
        // Assert
        Assertions.assertEquals(BUTTER, meatFarmerFood.getName());
        Assertions.assertEquals(1, meatFarmerFood.getQuantity());
        Assertions.assertEquals(-18, meatFarmerFood.getStorageTemperature());
    }
    /*
    * Tests that Meat Farmer is not able to process specific food
    * */
    @Test
    @Tag("UnitTest")
    public void MeatFarmerFailsToProcessFoodTest(){
        // Arrange
        Request request = new Request(meatFarmer, ONION, 1,  meatFarmer.getOperationType());
        // Act
        FoodEntity meatFarmerFood = meatFarmer.process(request);
        // Assert
        Assertions.assertNull(meatFarmerFood);
    }
    /*
    * Ensures that Meat Farmer operation is always GROW
    * */
    @Test
    @Tag("UnitTest")
    public void OperationIsAlwaysGrowTest(){
        // Act
        OperationEnum meatFarmerOperation = meatFarmer.getOperationType();
        // Assert
        Assertions.assertEquals(GROW, meatFarmerOperation);
    }
}
