package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import dataConvertors.ExpectedOutcomeConvertor;
import dataConvertors.IngredientListConvertor;
import dataConvertors.StringConvertor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import java.util.Arrays;
import java.util.List;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.EGGS;

public class FarmerTest {
    static TransactionInformer transactionInformer=  new TransactionInformer();
    static String farmerName = "KrepkijMuzik";
    static int cashAccount = 1500;

    /*
     * 2-way uniform strength coverage parameterized test for Farmer constructor
     * */
    @ParameterizedTest
    @Tag("ParameterizedTest")
    @CsvFileSource(resources = "/Farmer2Way.csv", numLinesToSkip = 1, delimiter = ',')
    public void TwoWayCoverageConstructorTest(@ConvertWith(StringConvertor.class)  String country,
                                              @ConvertWith(IngredientListConvertor.class) List<FoodEnum> ingredientList,
                                              int birthYear,
                                              @ConvertWith(ExpectedOutcomeConvertor.class) boolean expectedOutcome){

        // Arrange
        boolean hasPassed = true;
        // Act
        try {
            new MeatFarmer(farmerName, country, cashAccount, birthYear, ingredientList, transactionInformer);
            new VegetableFarmer(farmerName, country, cashAccount, birthYear, ingredientList, transactionInformer);
        }

        catch (Exception e){hasPassed = false;}
        // Assert
        Assertions.assertEquals(expectedOutcome, hasPassed);
    }
    /*
    * Checks if the farmer agrees to execute food entity correctly
    * */
    @Test
    @Tag("UnitTest")
    public void isAgreeToExecuteCorrectTest(){
        // Arrange
        MeatFarmer meatFarmer = new MeatFarmer(farmerName, "KrepkojeGosudarstvo", cashAccount,
                1990, Arrays.asList(MILK, MEAT, BUTTER, EGGS), transactionInformer);
        Request request1 = new Request(meatFarmer, MEAT, 1, meatFarmer.getOperationType());
        Request request2 = new Request(meatFarmer, WATER, 1, meatFarmer.getOperationType());
        // Act
        boolean agreesToExecute = meatFarmer.isAgreeToExecute(request1);
        boolean disagreesToExecute = meatFarmer.isAgreeToExecute(request2);
        // Assert
        Assertions.assertTrue(agreesToExecute);
        Assertions.assertFalse(disagreesToExecute);
    }

}
