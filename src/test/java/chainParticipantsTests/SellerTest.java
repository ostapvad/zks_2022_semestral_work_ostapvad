package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.Seller;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.SELL;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.ONION;

public class SellerTest {
    Seller seller;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants;

    @BeforeEach
    public void init() {
        channelParticipants = new ArrayList<>();
        transactionInformer = new TransactionInformer();
        seller = new Seller("Prodavec", 100, transactionInformer);
    }

    /*
    * Ensures that the Seller registers to all channels
    * */
    @Test
    @Tag("UnitTest")
    public void registerToChannelTest() {
        // Arrange
        channelParticipants.add(seller);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Act
        seller.registerToTheChannel();
        // Assert
        Assertions.assertTrue(MeatChannel.getMeatChannel().isRegister(seller));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(seller));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().isRegister(seller));
        Assertions.assertTrue(VegetableChannel.getVegetableChannel().isRegister(seller));
    }

    /*
    * Tests that the Seller OperationType is always SELL
    * */
    @Test
    @Tag("UnitTest")
    public void sellerOperationTypeTest() {
        // Arrange
        OperationEnum sellerOperationType = seller.getOperationType();
        // Assert
        Assertions.assertEquals(SELL, sellerOperationType);
    }

    /*
     * Ensures that Seller processes request correctly
     * */
    @Test
    @Tag("UnitTest")
    public void sellerProcessesCorrectlyTest(){
        // Arrange
        Request request = new Request(seller, MILK, 1, seller.getOperationType());
        FoodEntity milk = new FoodEntity(MILK, 1, -2);
        seller.addFood(milk);
        // Act
        FoodEntity processedFood = seller.process(request);
        // Assert
        Assertions.assertEquals(MILK, processedFood.getName());
        Assertions.assertEquals(1, processedFood.getQuantity());
        Assertions.assertEquals(-2, processedFood.getStorageTemperature());
    }


    /*
     * Tests that situation when Seller fails to process request
     * */
    @Test
    @Tag("UnitTest")
    public void sellerProcessFailsTest(){
        // Arrange
        Request request = new Request(seller, WATER, 1, seller.getOperationType());
        // Act
        FoodEntity processedFood = seller.process(request);
        // Assert
        Assertions.assertNull(processedFood);
    }
    /*
    * Tests that the product was added to seller correctly
    * */
    @Test
    @Tag("UnitTest")
    public void addProductToSellerCorrectlyTest() {
        // Arrange
        FoodEntity sugar = new FoodEntity(SUGAR, 1, 5);
        // Act
        seller.addFood(sugar);
        List<FoodEntity> sellerProducts = seller.getProducts();
        FoodEntity lastAddedProduct = sellerProducts.get(sellerProducts.size() - 1);
        List<String> productionActions = lastAddedProduct.getDoneActions();
        // Assert
        Assertions.assertEquals(1, seller.getProducts().size());
        Assertions.assertEquals(SUGAR, lastAddedProduct.getName());
        Assertions.assertEquals( 1, lastAddedProduct.getQuantity());
        Assertions.assertEquals( 5, lastAddedProduct.getStorageTemperature());
        Assertions.assertEquals(1, productionActions.size());
    }
}
