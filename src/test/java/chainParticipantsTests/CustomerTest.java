package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.CUSTOMER;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;


public class CustomerTest {
    TransactionInformer transactionInformer;
    Customer customer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants;

    @BeforeEach
    public void init(){
        channelParticipants =  new ArrayList<>();
        transactionInformer = new TransactionInformer();
        customer = new Customer("Loch", 555, transactionInformer);
    }

    /*
    * Checks if the new added food is correctly saved to customer's products
    * */
    @Test
    @Tag("UnitTest")
    public void addFoodTest(){
        // Arrange
        FoodEntity butter  = new FoodEntity(BUTTER, 3, 5);
        // Act
        customer.addFood(butter);
        List<FoodEntity> currentProducts = customer.getProducts();
        FoodEntity lastAddedFood = currentProducts.get(currentProducts.size() - 1);
        // Assert
        Assertions.assertEquals(BUTTER, lastAddedFood.getName());
        Assertions.assertEquals(3, lastAddedFood.getQuantity());
        Assertions.assertEquals(5, lastAddedFood.getStorageTemperature());

    }

    /*
     * Checks if the customer registered to all possible channels
     */
    @Test
    @Tag("UnitTest")
    public void registerToTheChannelTest(){
        // Arrange
        channelParticipants.add(customer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Act
        customer.registerToTheChannel();
        // Assert
        Assertions.assertTrue(MeatChannel.getMeatChannel().isRegister(customer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(customer));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().isRegister(customer));
        Assertions.assertTrue(VegetableChannel.getVegetableChannel().isRegister(customer));

    }

    /*
    * Simulates that the customer can't create any food
    * */
    @Test
    @Tag("UnitTest")
    public void processAlwaysFailsTest(){
        // Arrange
        Request request = new Request(customer, BEET, 1, customer.getOperationType());
        // Act
        FoodEntity processedFood = customer.process(request);
        // Assert
        Assertions.assertNull(processedFood);
    }

    /*
    * Simulates the fact that customer only buys product and has no prices
    * */
    @Test
    @Tag("UnitTest")
    public void customerHasNoPricesTest(){
        // Arrange
        FoodEntity draniki  = new FoodEntity(DRANIKI, 1, 1);
        // Act
        customer.addFood(draniki);
        int actualPrice = customer.askPrice(DRANIKI, 1);
        // Assert
        Assertions.assertEquals(0, actualPrice);
    }

   /*
   *  Simulates that customer can't execute any request
   * */
    @Test
    @Tag("UnitTest")
    public void customerAlwaysFailsToExecuteTest(){
        // Arrange
        Request request = new Request(customer, ICECREAM, 1, customer.getOperationType());
        // Act
        boolean isCustomerAgrees = customer.isAgreeToExecute(request);
        // Assert
        Assertions.assertFalse(isCustomerAgrees);
    }
    /*
    * Ensures that operation type is always CUSTOMER
    * */
    @Test
    @Tag("UnitTest")
    public void operationCorrectTest(){
        // Act
        OperationEnum customerOperation = customer.getOperationType();
        // Assert
        Assertions.assertEquals(CUSTOMER, customerOperation);
    }
}
