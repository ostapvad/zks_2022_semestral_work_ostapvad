package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.Storehouse;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.STORE;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class StoreHouseTest {
    Storehouse storehouse;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants;

    @BeforeEach
    public void init(){
        channelParticipants =  new ArrayList<>();
        transactionInformer = new TransactionInformer();
        storehouse = new Storehouse("Budka", 1000, transactionInformer);

    }

    /*
    * Ensures that Storehouse operation is always Store
    * */
    @Test
    @Tag("UnitTest")
    public void getOperationTypeTest(){
        // Act
        OperationEnum actualStorageOperation = storehouse.getOperationType();
        // Assert
        Assertions.assertEquals(STORE, actualStorageOperation);
    }

    /*
    * Tests that Storehouse registers to all channels
    * */
    @Test
    @Tag("UnitTest")
    public void registerToChannelTest(){
        // Arrange
        channelParticipants.add(storehouse);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Act
        storehouse.registerToTheChannel();
        // Assert
        Assertions.assertTrue(MeatChannel.getMeatChannel().isRegister(storehouse));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(storehouse));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().isRegister(storehouse));
        Assertions.assertTrue(VegetableChannel.getVegetableChannel().isRegister(storehouse));

    }


    /*
    * Ensures that the food is correctly added to the fridge
    * Temperature that is outside of limits [-20, 10] is checked separately with FoodEntity constructor
    * */
    @ParameterizedTest
    @Tag("ParameterizedTest")
    @CsvSource(
            {
             "-20, Cold",
             "-11, Cold",
             "-10, Medium",
             "-1, Medium",
             "0, Warm",
             "10, Warm"})
    public void addFoodToFridgeCorrectlyTest(int temperature, String targetFridge){
        // Arrange
        FoodEntity milk = new FoodEntity(MILK, 1, temperature);
        int expectedColdSize = 0, expectedMediumSize = 0, expectedWarmSize = 0;
        switch (targetFridge){
            case "Cold":
                expectedColdSize = 1;
                break;
            case "Medium":
                expectedMediumSize = 1;
                break;
            case "Warm":
                expectedWarmSize = 1;

        }

        // Act
        storehouse.addFood(milk);
        int actualColdSize = storehouse.getColdFridge().size();
        int actualMediumSize = storehouse.getMediumFridge().size();
        int actualWarmSize = storehouse.getWarmFridge().size();
        List <FoodEntity> allProducts = storehouse.getProducts();
        FoodEntity lastAddedProduct = allProducts.get(allProducts.size() -1);
        // Assert
        Assertions.assertEquals(expectedColdSize, actualColdSize);
        Assertions.assertEquals(expectedMediumSize, actualMediumSize);
        Assertions.assertEquals(expectedWarmSize, actualWarmSize);
        Assertions.assertEquals(MILK, lastAddedProduct.getName());
        Assertions.assertEquals(1, lastAddedProduct.getQuantity());
        Assertions.assertEquals(temperature, lastAddedProduct.getStorageTemperature());

    }

    /*
     * Ensures that StoreHouse is able to process request
     * */
    @Test
    @Tag("UnitTest")
    public void isAgreeToExecuteTest(){
        // Arrange
        FoodEntity potato = new FoodEntity(POTATO, 1, 3);
        Request request = new Request(storehouse, POTATO, 1, storehouse.getOperationType());
        storehouse.addFood(potato);
        // Act
        FoodEntity processedFood = storehouse.process(request);
        // Assert
        Assertions.assertEquals(POTATO, processedFood.getName());
        Assertions.assertEquals(1, processedFood.getQuantity());
        Assertions.assertEquals(3, processedFood.getStorageTemperature());

    }


    /*
    * Tests when the Storehouse fails to process request
    * */
    @Test
    @Tag("UnitTest")
    public void storageProcessFailsTest(){
        // Arrange
        Request request = new Request(storehouse, EGGS, 1, storehouse.getOperationType());
        // Act
        FoodEntity processedFood = storehouse.process(request);
        // Assert
        Assertions.assertNull(processedFood);
    }

}


