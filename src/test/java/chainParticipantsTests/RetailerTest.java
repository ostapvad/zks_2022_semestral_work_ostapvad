package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.Retailer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.RETAIL;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class RetailerTest {
    Retailer retailer;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants;
    @BeforeEach
    public void init(){
        channelParticipants = new ArrayList<>();
        transactionInformer = new TransactionInformer();
        retailer = new Retailer("Pavlik", 1500, transactionInformer);
    }
    /*
    * Ensures that Retailer registers to all channels
    * */
    @Test
    @Tag("UnitTest")
    public void registerToChannelTest(){
        // Arrange
        channelParticipants.add(retailer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Act
        retailer.registerToTheChannel();
        // Assert
        Assertions.assertTrue(MeatChannel.getMeatChannel().isRegister(retailer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(retailer));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().isRegister(retailer));
        Assertions.assertTrue(VegetableChannel.getVegetableChannel().isRegister(retailer));
    }

    /*
    * Ensures that Retailer Operation type is always RETAIL
    * */
    @Test
    @Tag("UnitTest")
    public void retailerOperationTypeTest(){
        // Act
        OperationEnum retailOperationType = retailer.getOperationType();
        // Assert
        Assertions.assertEquals(RETAIL, retailOperationType);
    }
    /*
    * Tests that Retailer is able to execute request
    * */
    @Test
    @Tag("UnitTest")
    public void RetailerAgreesToExecuteTest(){
         // Arrange
        Request request = new Request(retailer, BORSHCH, 1, retailer.getOperationType());
        FoodEntity borshch = new FoodEntity(BORSHCH, 1, 5);
        retailer.addFood(borshch);
        // Act
        boolean isAgreeToExecute = retailer.isAgreeToExecute(request);
        // Assert
        Assertions.assertTrue(isAgreeToExecute);
    }

    /*
    * Ensures that Retailer disagrees to execute request
    * */
    @Test
    @Tag("UnitTest")
    public void RetailerDisagreeToExecuteTest(){
        // Arrange
        Request request = new Request(retailer, DRANIKI, 1, retailer.getOperationType());
        // Act
        boolean isAgreeToExecute = retailer.isAgreeToExecute(request);
        // Assert
        Assertions.assertFalse(isAgreeToExecute);
    }
    /*
     * Ensures that the retail doesn't have requested food and can't process it
     * */
    @Test
    @Tag("UnitTest")
    public void retailerFailsToProcessFoodTest(){
        // Arrange
        Request request = new Request(retailer, WATER, 1, retailer.getOperationType());
        // Act
        FoodEntity processedFood = retailer.process(request);
        // Assert
        Assertions.assertNull(processedFood);
    }

    /*
    * Tests that retailer is able to process food
    * */
    @Test
    @Tag("UnitTest")
    public void retailProcessesFoodCorrectly(){
        // Arrange
        Request request = new Request(retailer, BEET, 1, retailer.getOperationType());
        FoodEntity beet = new FoodEntity(BEET, 1, -3);
        retailer.addFood(beet);
        // Act
        FoodEntity processedFood = retailer.process(request);
        // Assert
        Assertions.assertEquals(BEET, processedFood.getName());
        Assertions.assertEquals(1, processedFood.getQuantity());
        Assertions.assertEquals(-3, processedFood.getStorageTemperature());
    }
    /*
    * Ensures that the product ONION is added correctly
    * */
    @Test
    @Tag("UnitTest")
    public void addProductCorrectTest(){
        // Arrange
        FoodEntity onion = new FoodEntity(ONION, 4, 1);
        // Act
        retailer.addFood(onion);
        List<FoodEntity> currentProducts = retailer.getProducts();
        FoodEntity lastAddedFood = currentProducts.get(currentProducts.size() - 1);
        // Assert
        Assertions.assertEquals(ONION, lastAddedFood.getName());
        Assertions.assertEquals(4, lastAddedFood.getQuantity());
        Assertions.assertEquals(1, lastAddedFood.getStorageTemperature());
    }

}
