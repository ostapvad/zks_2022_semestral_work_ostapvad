package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import dataConvertors.ExpectedOutcomeConvertor;
import dataConvertors.StringConvertor;
import dataConvertors.TransactionInformerConvertor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import java.util.Arrays;
import java.util.List;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.EGGS;

public class ChainParticipantTest {

    /*
    * 3-way uniform strength coverage parameterized test for ChainParticipant constructor
    * */

    @Tag("ParameterizedTest")
    @ParameterizedTest
    @CsvFileSource(resources = "/ChainParticipant3Way.csv", numLinesToSkip = 1, delimiter = ',')
    public void ThreeWayCoverageConstructor(@ConvertWith(StringConvertor.class)  String name,
                                                       int cashAccount,
                                                       @ConvertWith(TransactionInformerConvertor.class)
                                                  TransactionInformer transactionInformer,
                                                       @ConvertWith(ExpectedOutcomeConvertor.class) boolean expectedOutcome){
        // Arrange
        boolean hasPassed = true;
        // Act
        try {new Customer(name, cashAccount, transactionInformer);}
        catch (Exception e){
            hasPassed = false;}
        // Assert
        Assertions.assertEquals(expectedOutcome, hasPassed);
    }

    /*
    * Checks if the price is updated correctly
    * */
    @Test
    @Tag("UnitTest")
    public void askPriceTest(){
        // Arrange
        FoodEnum expectedProduct = FoodEnum.BEET;
        int expectedPrice = 100;
        Retailer retailer = new Retailer("Baryga", 1000, new TransactionInformer());
        // Act
        retailer.put(expectedProduct, expectedPrice);
        int actualPrice = retailer.askPrice(expectedProduct, 1);
        // Assert
        Assertions.assertEquals(expectedPrice, actualPrice);
    }

    /*
    * Simulates if the request is performed correctly
    * */
    @Test
    @Tag("UnitTest")
    public void sendRequestTest(){
        // Arrange
        TransactionInformer transactionInformer = new TransactionInformer();
        List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);

        MeatFarmer meatFarmer = new MeatFarmer("Sergij", "Ukraine", 1500, 2000,
                meatIngredients, transactionInformer);
        Customer customer = new Customer("Abonent", 200, transactionInformer);
        List<Party> channelParticipants = Arrays.asList(meatFarmer, customer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(List.of(customer), List.of(POTATO), transactionInformer);
        ReadyMealChannel.getReadyMealChannel(List.of(customer), List.of(DRANIKI), transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants, transactionInformer);
        meatFarmer.registerToTheChannel();
        customer.registerToTheChannel();
        FoodEnum butter = BUTTER; // can be requested
        FoodEnum beet = BEET; // can't be requested
        // Act
        customer.sendRequest(butter, 1);
        customer.sendRequest(beet, 1);
        // Assert
        Assertions.assertTrue(customer.getProducts().stream().anyMatch(food->food.getName().equals(butter)
                && food.getQuantity() == 1));
        Assertions.assertFalse(customer.getProducts().stream().anyMatch(food->food.getName().equals(beet)));

    }

    /*
    * Check if double spending problem detected - when two transactions have the same hash
    * */
    @Test
    @Tag("UnitTest")
    public void correctDoubleSpendingTest(){
        // Arrange
        TransactionInformer transactionInformer =  new TransactionInformer();
        List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        Customer customer = new Customer("Abonent", 200, transactionInformer);
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Vasilij", "Russia", 5450,
                1995, vegetableIngredients, transactionInformer);
        List<Party> channelParticipants = Arrays.asList(vegetableFarmer, customer);
        MeatChannel.getMeatChannel(channelParticipants, List.of(MEAT), transactionInformer);
        VegetableChannel.getVegetableChannel(List.of(customer), vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(List.of(customer), List.of(BORSHCH), transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants, transactionInformer);
        customer.registerToTheChannel();
        vegetableFarmer.registerToTheChannel();
        Request request = new Request(customer, WATER, 1, vegetableFarmer.getOperationType());
        // Act
        Transaction receivedTransaction = new Transaction(request, vegetableFarmer, customer.getLastHash());
        customer.updateTransactions(receivedTransaction);
        int totalDoubleSpending =  customer.doubleSpending(receivedTransaction);
        // Assert
        Assertions.assertEquals(1, totalDoubleSpending);

    }

}
