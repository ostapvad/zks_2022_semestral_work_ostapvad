package chainParticipantsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.*;
import java.util.*;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.PRODUCE;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;


public class ManufacturerTest {
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    TransactionInformer transactionInformer;
    Manufacturer manufacturer;
    Customer customer;
    List<Party> channelParticipants;
    @BeforeEach
    public void init(){
        channelParticipants =  new ArrayList<>();
        transactionInformer = new TransactionInformer();
        customer = new Customer("Klient", 10000, transactionInformer);
        manufacturer = new Manufacturer("Tomas", 5000, FoodEnum.BORSHCH, transactionInformer);
    }

    /*
    * Ensures that manufacturer registered to all channels correctly
    * */
    @Test
    @Tag("UnitTest")
    public void registerToTheChannelCorrectTest(){
        // Arrange
        channelParticipants.add(manufacturer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Act
        manufacturer.registerToTheChannel();
        // Assert
        Assertions.assertTrue(MeatChannel.getMeatChannel().isRegister(manufacturer));
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(manufacturer));
        Assertions.assertTrue(ReadyMealChannel.getReadyMealChannel().isRegister(manufacturer));
        Assertions.assertTrue(VegetableChannel.getVegetableChannel().isRegister(manufacturer));

    }

    /*
    * Ensures that borshsh will be prepared by the customer request
    */
    @Test
    @Tag("UnitTest")
    public void manufacturerProcessCorrectTest(){
        // Arrange
        Collections.addAll(channelParticipants, manufacturer, customer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        manufacturer.registerToTheChannel();
        customer.registerToTheChannel();

        manufacturer.addFood(new FoodEntity(BEET, 1, 2));
        manufacturer.addFood(new FoodEntity(WATER, 1, 4));
        manufacturer.addFood(new FoodEntity(POTATO, 1, 1));
        manufacturer.addFood(new FoodEntity(MEAT, 1, -2));
        Request request = new Request(customer, BORSHCH, 1, manufacturer.getOperationType());
        // Act
        FoodEntity preparedProduct = manufacturer.process(request);
        // Assert
        Assertions.assertEquals(preparedProduct.getName(), BORSHCH);
        Assertions.assertEquals(preparedProduct.getQuantity(), 1);

    }

    /*
    * Tests that draniki will not be prepared because one of the ingredients wasn't added to the manufacturer
    */
    @Test
    @Tag("UnitTest")
    public void manufacturerProcessIngredientFailTest(){
        // Arrange
        Collections.addAll(channelParticipants, manufacturer, customer);
        MeatChannel.getMeatChannel(channelParticipants, meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        manufacturer.registerToTheChannel();
        customer.registerToTheChannel();
        // Arrange
        manufacturer.addFood(new FoodEntity(POTATO, 1, -4));
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 1));
        manufacturer.addFood(new FoodEntity(EGGS, 1, -5));
        Request request = new Request(customer, DRANIKI, 1, manufacturer.getOperationType());
        // Act
        FoodEntity preparedProduct =manufacturer.process(request);
        // Assert
        Assertions.assertNull(preparedProduct);

    }

    /*
     * Ensures that operation type is always PRODUCE
     */
    @Test
    @Tag("UnitTest")
    public void operationCorrectTest(){
        // Act
        OperationEnum manufacturerOperation = manufacturer.getOperationType();
        // Assert
        Assertions.assertEquals(PRODUCE, manufacturerOperation);
    }

}

