package strategyTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookDraniki;
import cz.cvut.fel.omo.foodChain.strategy.CookIceCream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.DRANIKI;

public class CookIceCreamTest {

    Manufacturer manufacturer;
    CookIceCream cookIceCream;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants = new ArrayList<>();

    HashMap<FoodEnum, FoodEntity> iceCreamReceipt = new HashMap<>() {{
        put(MILK, new FoodEntity(MILK, 1, -5));
        put(SUGAR, new FoodEntity(SUGAR, 1, 4));

    }};
    @BeforeEach
    void init(){
        transactionInformer = new TransactionInformer();
        manufacturer = new Manufacturer("Petro", 350, ICECREAM, transactionInformer);
        channelParticipants.add(manufacturer);
        MeatChannel.getMeatChannel(channelParticipants,  meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants ,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants , readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants , vegetableIngredients, transactionInformer);

        manufacturer.registerToTheChannel();
        cookIceCream = new CookIceCream(manufacturer);
    }
    /*
    * Checks if the default iceCream receipt is correct
    * */
    @Test
    @Tag("UnitTest")
    public void isIceCreamReceiptCorrectTest(){
        // Act
        HashMap<FoodEnum, FoodEntity> defaultReceipt = cookIceCream.getReceipt();
        int totalIngredients = 0;
        int totalNullFoodEntities = 0;
        for (FoodEnum ingredient: defaultReceipt.keySet()){
            if(iceCreamReceipt.containsKey(ingredient)){
                totalIngredients ++;
                if(defaultReceipt.get(ingredient) == null) totalNullFoodEntities ++;
            }

        }
        // Assert
        Assertions.assertEquals(defaultReceipt.size(),iceCreamReceipt.size());
        Assertions.assertEquals(totalIngredients, iceCreamReceipt.size());
        Assertions.assertEquals(totalNullFoodEntities, iceCreamReceipt.size());

    }

    /*
    *  Tests that CookIceCream strategy can only be assigned to not null manufacturer
    * */
    @Test
    @Tag("UnitTest")
    public void iceCreamManufacturerCorrectAssignmentTest(){
        // Act
        Manufacturer actualManufacturer = cookIceCream.getManufacturer();
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->new CookIceCream(null));
        Assertions.assertEquals(manufacturer, actualManufacturer);
    }

    /*
    * Ensures that prepared iceCream is correct
    * */
    @Test
    @Tag("UnitTest")
    public void IceCreamCookedCorrectlyTest() {
        // Arrange
        FoodEntity expectedIceCream = new FoodEntity(ICECREAM, 1, -5);
        for (FoodEnum ingredient : iceCreamReceipt.keySet())
            manufacturer.addFood(iceCreamReceipt.get(ingredient));
        // Act
        FoodEntity preparedMeal = cookIceCream.cook();
        // Assert
        Assertions.assertEquals(expectedIceCream.getName(), preparedMeal.getName());
        Assertions.assertEquals(expectedIceCream.getQuantity(), preparedMeal.getQuantity());
        Assertions.assertEquals(expectedIceCream.getStorageTemperature(), preparedMeal.getStorageTemperature());
    }

    /*
     * Ensures that iceCream can't be cooked due to the lack ingredients
     * */
    @Test
    @Tag("UnitTest")
    public void impossibleToCookIceCreamTest() {
        // Arrange
        int counter = 0;
        for (FoodEnum ingredient : iceCreamReceipt.keySet()){
            if(counter == iceCreamReceipt.size() - 1) break;
            manufacturer.addFood(iceCreamReceipt.get(ingredient));
            counter++;
        }
        // Act
        FoodEntity preparedMeal = cookIceCream.cook();
        // Assert
        Assertions.assertNull(preparedMeal);
    }

}
