package strategyTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookKyivCutlet;
import cz.cvut.fel.omo.foodChain.strategy.CookPancakes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.KYIVCUTLET;

public class CookPancakesTest {
    Manufacturer manufacturer;
    CookPancakes cookPancakes;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants = new ArrayList<>();

    HashMap<FoodEnum, FoodEntity> pancakesReceipt = new HashMap<>() {{
        put(FLOUR, new FoodEntity(FLOUR, 1, 1));
        put(SUGAR, new FoodEntity(SUGAR, 1, 4));
        put(MILK, new FoodEntity(MILK, 1, -7));

    }};
    @BeforeEach
    void init(){
        transactionInformer = new TransactionInformer();
        manufacturer = new Manufacturer("Petro", 350, KYIVCUTLET, transactionInformer);
        channelParticipants.add(manufacturer);
        MeatChannel.getMeatChannel(channelParticipants,  meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants ,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants , readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants , vegetableIngredients, transactionInformer);
        manufacturer.registerToTheChannel();
        cookPancakes = new CookPancakes(manufacturer);
    }
    /*
    * Tests if pancakes receipt is correct in CookPancakes strategy
    * */
    @Test
    @Tag("UnitTest")
    public void isPancakesReceiptCorrect(){
        // Act
        HashMap<FoodEnum, FoodEntity> defaultReceipt = cookPancakes.getReceipt();
        int totalIngredients = 0;
        int totalNullFoodEntities = 0;
        for (FoodEnum ingredient: defaultReceipt.keySet()){
            if(pancakesReceipt.containsKey(ingredient)){
                totalIngredients ++;
                if(defaultReceipt.get(ingredient) == null) totalNullFoodEntities ++;
            }

        }
        // Assert
        Assertions.assertEquals(defaultReceipt.size(),pancakesReceipt.size());
        Assertions.assertEquals(totalIngredients, pancakesReceipt.size());
        Assertions.assertEquals(totalNullFoodEntities, pancakesReceipt.size());
    }

    /*
    * Ensures that Pancakes strategy can only be assigned to not null manufacturer
    * */
    @Test
    @Tag("UnitTest")
    public void PancakesManufacturerCorrectAssignmentTest(){
        // Act
        Manufacturer actualManufacturer = cookPancakes.getManufacturer();
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->new CookPancakes(null));
        Assertions.assertEquals(manufacturer, actualManufacturer);
    }
    /*
    * Ensures that Pancakes prepared correctly
    * */
    @Test
    @Tag("UnitTest")
    public void PancakesCorrectlyCookedTest() {
        // Arrange
        FoodEntity expectedPancakes = new FoodEntity(PANCAKES, 1, 5);
        for (FoodEnum ingredient : pancakesReceipt.keySet())
            manufacturer.addFood(pancakesReceipt.get(ingredient));
        // Act
        FoodEntity preparedMeal = cookPancakes.cook();
        // Assert
        Assertions.assertEquals(expectedPancakes.getName(), preparedMeal.getName());
        Assertions.assertEquals(expectedPancakes.getQuantity(), preparedMeal.getQuantity());
        Assertions.assertEquals(expectedPancakes.getStorageTemperature(), preparedMeal.getStorageTemperature());
    }

    /*
     * Ensures that Pancakes can't be cooked due to the lack ingredients
     * */
    @Test
    @Tag("UnitTest")
    public void PancakesAreNotPreparedTest() {
        // Arrange
        int counter = 0;
        for (FoodEnum ingredient : pancakesReceipt.keySet()) {
            if (counter == pancakesReceipt.size() - 1) break;
            manufacturer.addFood(pancakesReceipt.get(ingredient));
            counter++;
        }
        // Act
        FoodEntity preparedMeal = cookPancakes.cook();
        // Assert
        Assertions.assertNull(preparedMeal);
    }
}
