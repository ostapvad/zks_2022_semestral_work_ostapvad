package strategyTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookIceCream;
import cz.cvut.fel.omo.foodChain.strategy.CookKyivCutlet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.ICECREAM;

public class CookKyivCutletTest {
    Manufacturer manufacturer;
    CookKyivCutlet cookKyivCutlet;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants = new ArrayList<>();

    HashMap<FoodEnum, FoodEntity> KyivCutletReceipt = new HashMap<>() {{
        put(MEAT, new FoodEntity(MEAT, 1, -3));
        put(FLOUR, new FoodEntity(FLOUR, 1, 2));
        put(BUTTER, new FoodEntity(BUTTER, 1, -4));

    }};


    @BeforeEach
    void init(){
        transactionInformer = new TransactionInformer();
        manufacturer = new Manufacturer("Petro", 350, KYIVCUTLET, transactionInformer);
        channelParticipants.add(manufacturer);
        MeatChannel.getMeatChannel(channelParticipants,  meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants ,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants , readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants , vegetableIngredients, transactionInformer);
        manufacturer.registerToTheChannel();
        cookKyivCutlet = new CookKyivCutlet(manufacturer);
    }

    /*
    * Checks if the default receipt of KyivCotlet is correct
    * */
    @Test
    @Tag("UnitTest")
    public void isKyivCutletReceiptCorrectTest(){
        // Act
        HashMap<FoodEnum, FoodEntity> defaultReceipt = cookKyivCutlet.getReceipt();
        int totalIngredients = 0;
        int totalNullFoodEntities = 0;
        for (FoodEnum ingredient: defaultReceipt.keySet()){
            if(KyivCutletReceipt.containsKey(ingredient)){
                totalIngredients ++;
                if(defaultReceipt.get(ingredient) == null) totalNullFoodEntities ++;
            }
        }
        // Assert
        Assertions.assertEquals(defaultReceipt.size(),KyivCutletReceipt.size());
        Assertions.assertEquals(totalIngredients, KyivCutletReceipt.size());
        Assertions.assertEquals(totalNullFoodEntities, KyivCutletReceipt.size());

    }

    /*
     *  Tests that KyivCutlet strategy can only be assigned to not null manufacturer
     * */
    @Test
    @Tag("UnitTest")
    public void KyivCutletManufacturerCorrectAssignmentTest(){
        // Act
        Manufacturer actualManufacturer = cookKyivCutlet.getManufacturer();
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->new CookKyivCutlet(null));
        Assertions.assertEquals(manufacturer, actualManufacturer);
    }

    /*
    * Ensures that KyivCutlet is prepared correctly
    * */
    @Test
    @Tag("UnitTest")
    public void KyivCutletCooksCorrectlyTest() {
        // Arrange
        FoodEntity expectedKyivCutlet = new FoodEntity(KYIVCUTLET, 1, 8);
        for (FoodEnum ingredient : KyivCutletReceipt.keySet())
            manufacturer.addFood(KyivCutletReceipt.get(ingredient));
        // Act
        FoodEntity preparedMeal = cookKyivCutlet.cook();
        // Assert
        Assertions.assertEquals(expectedKyivCutlet.getName(), preparedMeal.getName());
        Assertions.assertEquals(expectedKyivCutlet.getQuantity(), preparedMeal.getQuantity());
        Assertions.assertEquals(expectedKyivCutlet.getStorageTemperature(), preparedMeal.getStorageTemperature());
    }

    /*
     * Ensures that KyivCutlet can't be cooked due to the lack ingredients
     * */
    @Test
    @Tag("UnitTest")
    public void KyivCutletCanNotBeCookedTest(){
        // Arrange
        int counter = 0;
        for (FoodEnum ingredient : KyivCutletReceipt.keySet()){
            if(counter == KyivCutletReceipt.size() - 1) break;
            manufacturer.addFood(KyivCutletReceipt.get(ingredient));
            counter++;
        }
        // Act
        FoodEntity preparedMeal = cookKyivCutlet.cook();
        // Assert
        Assertions.assertNull(preparedMeal);
    }
}
