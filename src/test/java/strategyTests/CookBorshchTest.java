package strategyTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookBorshch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.*;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.MEAT;

public class CookBorshchTest {

    Manufacturer manufacturer;
    CookBorshch cookBorshch;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants = new ArrayList<>();
    HashMap<FoodEnum, FoodEntity> borshchReceipt = new HashMap<>() {{
        put(BEET, new FoodEntity(BEET, 1, 5));
        put(WATER, new FoodEntity(WATER, 1, 2));
        put(POTATO, new FoodEntity(POTATO, 1, 4));
        put(MEAT, new FoodEntity(MEAT, 1, -3));
    }};

    @BeforeEach
    void init(){
            transactionInformer = new TransactionInformer();
            manufacturer = new Manufacturer("Petro", 350, BORSHCH, transactionInformer);
            channelParticipants.add(manufacturer);
            MeatChannel.getMeatChannel(channelParticipants,  meatIngredients, transactionInformer);
            PaymentChannel.getPaymentChannel(channelParticipants ,  transactionInformer);
            ReadyMealChannel.getReadyMealChannel(channelParticipants , readyMeals, transactionInformer);
            VegetableChannel.getVegetableChannel(channelParticipants , vegetableIngredients, transactionInformer);
            manufacturer.registerToTheChannel();
            cookBorshch = new CookBorshch(manufacturer);

    }

    /*
    * Ensures that Borshch Receipt: ingredients are correct
    * */
    @Test
    @Tag("UnitTest")
    public void isBorshchReceiptCorrectTest(){
        // Act
        HashMap<FoodEnum, FoodEntity> defaultReceipt = cookBorshch.getReceipt();
        int totalIngredients = 0;
        int totalNullFoodEntities = 0;
        for (FoodEnum ingredient: defaultReceipt.keySet()){
            if(borshchReceipt.containsKey(ingredient)){
                totalIngredients ++;
                if(defaultReceipt.get(ingredient) == null) totalNullFoodEntities ++;
            }

        }
        // Assert
        Assertions.assertEquals(defaultReceipt.size(), borshchReceipt.size());
        Assertions.assertEquals(totalIngredients, borshchReceipt.size());
        Assertions.assertEquals(totalNullFoodEntities, borshchReceipt.size());

    }

    /*
    * Tests that cookBorshch can't be done for null Manufacturer; otherwise assigns Manufacturer
    * */
    @Test
    @Tag("UnitTest")
    public void borshchManufacturerCorrectAssignmentTest(){
        // Act
        Manufacturer actualManufacturer = cookBorshch.getManufacturer();
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->new CookBorshch(null));
        Assertions.assertEquals(manufacturer, actualManufacturer);
    }
    /*
     * Tests that cookBorshch can't be done for null Manufacturer; otherwise assigns Manufacturer
     * */
    @Test
    @Tag("UnitTest")
    public void borshchCanBeCookedTest() {
        // Arrange
        FoodEntity expectedBorshsh = new FoodEntity(BORSHCH, 1, 5);
        for (FoodEnum ingredient : borshchReceipt.keySet())
            manufacturer.addFood(borshchReceipt.get(ingredient));
        // Act
        FoodEntity preparedMeal = cookBorshch.cook();
        // Assert
        Assertions.assertEquals(expectedBorshsh.getName(), preparedMeal.getName());
        Assertions.assertEquals(expectedBorshsh.getQuantity(), preparedMeal.getQuantity());
        Assertions.assertEquals(expectedBorshsh.getStorageTemperature(), preparedMeal.getStorageTemperature());
    }

    /*
    * Tests that borsch was failed to cook due to the lack of ingredients
    * */
    @Test
    @Tag("UnitTest")
    public void borshchFailedToCookTest() {
        // Arrange
        int counter = 0;
        for (FoodEnum ingredient : borshchReceipt.keySet()) {
            if (counter == borshchReceipt.size() - 1) break;
            manufacturer.addFood(borshchReceipt.get(ingredient));
            counter++;
        }
        // Act
        FoodEntity preparedMeal = cookBorshch.cook();
        // Assert
        Assertions.assertNull(preparedMeal);
    }
}

