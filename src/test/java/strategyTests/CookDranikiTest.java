package strategyTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.strategy.CookBorshch;
import cz.cvut.fel.omo.foodChain.strategy.CookDraniki;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.BORSHCH;

public class CookDranikiTest {

    Manufacturer manufacturer;
    CookDraniki cookDraniki;
    TransactionInformer transactionInformer;
    List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    List<Party> channelParticipants = new ArrayList<>();
    HashMap<FoodEnum, FoodEntity> dranikiReceipt = new HashMap<>() {{
        put(POTATO, new FoodEntity(POTATO, 1, 6));
        put(ONION, new FoodEntity(ONION, 1, 1));
        put(FLOUR, new FoodEntity(FLOUR, 1, 7));
        put(EGGS, new FoodEntity(EGGS, 1, -2));
    }};

    @BeforeEach
    void init(){
        transactionInformer = new TransactionInformer();
        manufacturer = new Manufacturer("Petro", 350, DRANIKI, transactionInformer);
        channelParticipants.add(manufacturer);
        MeatChannel.getMeatChannel(channelParticipants,  meatIngredients, transactionInformer);
        PaymentChannel.getPaymentChannel(channelParticipants ,  transactionInformer);
        ReadyMealChannel.getReadyMealChannel(channelParticipants , readyMeals, transactionInformer);
        VegetableChannel.getVegetableChannel(channelParticipants , vegetableIngredients, transactionInformer);

        manufacturer.registerToTheChannel();
        cookDraniki = new CookDraniki(manufacturer);
    }

    /*
    * Checks if the default draniki receipt is set correctly
    * */
    @Test
    @Tag("UnitTest")
    public void isDranikiReceiptCorrectTest(){
        // Act
        HashMap<FoodEnum, FoodEntity> defaultReceipt = cookDraniki.getReceipt();
        int totalIngredients = 0;
        int totalNullFoodEntities = 0;
        for (FoodEnum ingredient: defaultReceipt.keySet()){
            if(dranikiReceipt.containsKey(ingredient)){
                totalIngredients ++;
                if(defaultReceipt.get(ingredient) == null) totalNullFoodEntities ++;
            }

        }
        // Assert
        Assertions.assertEquals(defaultReceipt.size(),dranikiReceipt.size());
        Assertions.assertEquals(totalIngredients, dranikiReceipt.size());
        Assertions.assertEquals(totalNullFoodEntities, dranikiReceipt.size());

    }

    /*
    * Tests that strategy can only be assigned to not null manufacturer
    * */
    @Test
    @Tag("UnitTest")
    public void dranikiManufacturerCorrectAssignmentTest(){
        // Act
        Manufacturer actualManufacturer = cookDraniki.getManufacturer();
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->new CookDraniki(null));
        Assertions.assertEquals(manufacturer, actualManufacturer);
    }

    /*
    * Simulates the process when the draniki can be cooked
    * */
    @Test
    @Tag("UnitTest")
    public void dranikiCookedCorrectlyTest() {
        // Arrange
        FoodEntity expectedDraniki = new FoodEntity(DRANIKI, 1, 8);
        for (FoodEnum ingredient : dranikiReceipt.keySet())
            manufacturer.addFood(dranikiReceipt.get(ingredient));
        // Act
        FoodEntity preparedMeal = cookDraniki.cook();
        // Assert
        Assertions.assertEquals(expectedDraniki.getName(), preparedMeal.getName());
        Assertions.assertEquals(expectedDraniki.getQuantity(), preparedMeal.getQuantity());
        Assertions.assertEquals(expectedDraniki.getStorageTemperature(), preparedMeal.getStorageTemperature());
    }

    /*
    * Ensures that draniki can't be cooked due to the lack ingredients
    * */
    @Test
    @Tag("UnitTest")
    public void dranikiCannotBePreparedTest() {
        // Arrange
        int counter = 0;
        for (FoodEnum ingredient : dranikiReceipt.keySet()){
            if(counter == dranikiReceipt.size() - 1) break;
            manufacturer.addFood(dranikiReceipt.get(ingredient));
            counter++;
        }
        // Act
        FoodEntity preparedMeal = cookDraniki.cook();
        // Assert
        Assertions.assertNull(preparedMeal);
    }

}
