package operationsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import dataConvertors.ExpectedOutcomeConvertor;
import dataConvertors.FoodEnumConvertor;
import dataConvertors.StringConvertor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.security.PublicKey;
import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;

public class TransactionTest {
    TransactionInformer transactionInformer;
    Request defaultRequest;
    VegetableFarmer defaultExecutor;
    Customer customer;
    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    @BeforeEach
    public void init(){
        transactionInformer = new TransactionInformer();
        customer = new Customer("Loch", 555, transactionInformer);
        defaultExecutor = new VegetableFarmer("Mykola", "Ukraine", 500, 1976,
                vegetableIngredients, transactionInformer);
        defaultRequest = new Request(customer, FoodEnum.ONION,5, customer.getOperationType());

    }
    /*
    * Performs the test of Transaction constructor with 3-Way uniform strength coverage
    * */
    @ParameterizedTest
    @Tag("ParametrizedTest")
    @CsvFileSource(resources = "/Transaction3Way.csv", numLinesToSkip = 1, delimiter = ',')
    public void Transaction3WayConstructorTest(@ConvertWith(StringConvertor.class) String request,
                                       @ConvertWith(StringConvertor.class) String executor, int previousHash,
                                       @ConvertWith(ExpectedOutcomeConvertor.class)
                                       boolean expectedOutcome){
        // Arrange
        if(request == null) defaultRequest = null;
        if(executor == null) defaultExecutor = null;
        boolean hasPassed = true;
        // Act
        try {new Transaction(defaultRequest, defaultExecutor, previousHash);}
        catch (Exception e){
            hasPassed = false;}
        // Assert
        Assertions.assertEquals(expectedOutcome, hasPassed);
    }

}
