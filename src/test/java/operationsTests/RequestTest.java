package operationsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Farmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import dataConvertors.ExpectedOutcomeConvertor;
import dataConvertors.FoodEnumConvertor;
import dataConvertors.StringConvertor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.EGGS;

public class RequestTest {
    static List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    static TransactionInformer transactionInformer;

    MeatFarmer defaultApplicant;
    OperationEnum testedOperationType;

    @BeforeEach
    public void init(){
        transactionInformer = new TransactionInformer();
        defaultApplicant = new MeatFarmer("Padlo", "Poland", 150, 2000, meatIngredients,
                transactionInformer);
        testedOperationType = defaultApplicant.getOperationType();

    }
    /*
    * Performs the mixed-strength coverage parameterized test of Request constructor
    * */
    @ParameterizedTest
    @Tag("ParametrizedTest")
    @CsvFileSource(resources = "/RequestMixed.csv", numLinesToSkip = 1, delimiter = ',')
    public void MixedRequestConstructorTest(@ConvertWith(StringConvertor.class) String applicant,
                                            @ConvertWith(FoodEnumConvertor.class) FoodEnum foodEnum,
                                            int quantity, @ConvertWith(StringConvertor.class) String operationType,
                                            @ConvertWith(ExpectedOutcomeConvertor.class) boolean expectedOutcome){
        // Arrange
        if(applicant == null) defaultApplicant = null;
        if(operationType == null) testedOperationType = null;
        boolean hasPassed = true;
        // Act
        try {new Request(defaultApplicant, foodEnum, quantity, testedOperationType);}
        catch (Exception e){
            hasPassed = false;}
        // Assert
        Assertions.assertEquals(expectedOutcome, hasPassed);
    }


}
