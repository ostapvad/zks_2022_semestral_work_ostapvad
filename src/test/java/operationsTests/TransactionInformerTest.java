package operationsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class TransactionInformerTest {
    /*
    * Tests that hash of transaction remains the same for applicant when notify method is called
    * */
    @Test
    @Tag("UnitTest")
    public void notifyPerformsCorrectlyTest(){
        // Arrange
        TransactionInformer transactionInformer =  new TransactionInformer();
        Customer customer = new Customer("Padlo", 100, transactionInformer);
        MeatFarmer meatFarmer = new MeatFarmer("Mudak", "USA", 1000,
                1990, List.of(MEAT), transactionInformer);
        Request request = new Request(customer, MEAT, 1, customer.getOperationType());
        Transaction transaction = new Transaction(request, meatFarmer, customer.getLastHash());
        int expectedHash = transaction.getHash();
        // Act
        transactionInformer.attach(customer);
        transactionInformer.notify(transaction);
        // Assert
        Assertions.assertEquals(expectedHash, customer.getLastHash());

    }
}
