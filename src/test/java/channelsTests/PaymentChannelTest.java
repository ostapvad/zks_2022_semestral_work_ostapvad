package channelsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Farmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;

import static cz.cvut.fel.omo.foodChain.channels.MeatChannel.getMeatChannel;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class PaymentChannelTest {
    TransactionInformer transactionInformer;
    Customer customer;
    MeatFarmer meatFarmer;
    List<Party> channelParticipants = new ArrayList<>();
    @BeforeEach
    public void init(){
        transactionInformer = new TransactionInformer();
        customer = new Customer("Pokupatel", 50, transactionInformer);
        meatFarmer = new MeatFarmer("Petrovich", "Ukraine", 100, 1990,
                List.of(FoodEnum.MEAT), transactionInformer);
        channelParticipants.add(customer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
    }
    /*
     * Tests that TransactionInformer can't be null
     * */
    @Test
    @Tag("UnitTest")
    public void PaymentChannelConstructorFails(){
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                ()-> PaymentChannel.getPaymentChannel(channelParticipants, null));
    }
    /*
    * Tests that PaymentChannel registration is done correctly
    * */
    @Test
    @Tag("UnitTest")
    public void channelRegistrationPerformsCorrectlyTest(){
        // Act
        PaymentChannel.getPaymentChannel().register(customer);
        // Assert
        Assertions.assertTrue(PaymentChannel.getPaymentChannel().isRegister(customer));
        Assertions.assertFalse(PaymentChannel.getPaymentChannel().isRegister(meatFarmer));
    }

    /*
     * Tests that unregister user performs correctly:
     * registered or unregistered user is unregistered
     * */
    @Test
    @Tag("UnitTest")
    public void UnregisterUserTest(){
        // Arrange
        PaymentChannel.getPaymentChannel().register(customer);
        // Act
        PaymentChannel.getPaymentChannel().unregister(customer);
        PaymentChannel.getPaymentChannel().unregister(meatFarmer);
        // Assert
        Assertions.assertFalse(PaymentChannel.getPaymentChannel().isRegister(customer));
        Assertions.assertFalse(PaymentChannel.getPaymentChannel().isRegister(meatFarmer));
    }
    /*
    * Tests the meatFarmer receives money from Customer, performs correctly
    * */
    @Test
    @Tag("UnitTest")
    public void makePaymentPerformsCorrectlyTest(){
        // Arrange
        channelParticipants.add(meatFarmer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        PaymentChannel.getPaymentChannel().register(customer);
        PaymentChannel.getPaymentChannel().register(meatFarmer);
        // Act
        PaymentChannel.getPaymentChannel().makePayment(customer, meatFarmer, 50);
        // Assert
        Assertions.assertEquals(150, meatFarmer.getCashAccount());
        Assertions.assertEquals(0, customer.getCashAccount());
    }

    /*
     * Tests that Customer fails to pay due to the lack of money
     * */
    @Test
    @Tag("UnitTest")
    public void makePaymentFailsTest(){
        // Arrange
        Customer poorCustomer = new Customer("Bedolaga", 0, transactionInformer);
        Collections.addAll(channelParticipants, poorCustomer, meatFarmer);
        PaymentChannel.getPaymentChannel(channelParticipants,  transactionInformer);
        PaymentChannel.getPaymentChannel().register(poorCustomer);
        PaymentChannel.getPaymentChannel().register(meatFarmer);
        // Act
        PaymentChannel.getPaymentChannel().makePayment(poorCustomer, meatFarmer, 50);
        // Assert
        Assertions.assertEquals(0, poorCustomer.getCashAccount());
        Assertions.assertEquals(100, meatFarmer.getCashAccount());

    }
}
