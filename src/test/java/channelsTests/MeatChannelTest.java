package channelsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Farmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;
import dataConvertors.ExpectedOutcomeConvertor;
import dataConvertors.FoodEnumConvertor;
import dataConvertors.StringConvertor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;
import static cz.cvut.fel.omo.foodChain.channels.MeatChannel.getMeatChannel;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.EGGS;

public class MeatChannelTest {
    Customer customer;
    Farmer farmer;
    TransactionInformer transactionInformer ;
    List<Party> channelParticipants = new ArrayList<>();
    List<FoodEnum> meatChannelIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    @BeforeEach
    public void init(){
        transactionInformer = new TransactionInformer();
        customer = new Customer("Pokupatel", 555, transactionInformer);
        farmer = new MeatFarmer("Peter", "UK", 1000, 1990, meatChannelIngredients,
                transactionInformer);
    }

    /*
    * Tests that TransactionInformer can't be null
    * */
    @Test
    @Tag("UnitTest")
    public void MeatChannelConstructorFails(){
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->getMeatChannel(channelParticipants,
                meatChannelIngredients,null));
    }

    /*
    * Ensures the Allowed entities are added correctly
    * */
    @Test
    @Tag("UnitTest")
    public void allowedEntitiesPerformsCorrectlyTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer, farmer);
        // Act
        getMeatChannel(channelParticipants, meatChannelIngredients, transactionInformer);
        // Assert
        Assertions.assertEquals(getMeatChannel().getAllowedEntities(), meatChannelIngredients);
    }
    /*
    * Ensures that participants is registered to channel correctly
    * */
    @Test
    @Tag("UnitTest")
    public void registrationMeatChannelPerformsCorrectlyTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer);
        // Act
        getMeatChannel(channelParticipants, meatChannelIngredients, transactionInformer);
        // Assert
        getMeatChannel().register(customer);
        getMeatChannel().register(farmer);
        Assertions.assertTrue(getMeatChannel().isRegister(customer));
        Assertions.assertFalse(getMeatChannel().isRegister(farmer));
    }
    /*
    * Tests that findExecutor method performs correctly
    * */
    @Test
    @Tag("UnitTest")
    public void findExecutorMeatChannelPerformsCorrectlyTest(){
        // Arrange
        Collections.addAll(channelParticipants, farmer);
        getMeatChannel(channelParticipants, meatChannelIngredients, transactionInformer);
        getMeatChannel().register(farmer);
        Request request  = new Request(farmer, MEAT, 1, farmer.getOperationType());
        // Act
        Party executor = getMeatChannel().findExecutor(request);
        // Assert
        Assertions.assertEquals(farmer, executor);
    }
    /*
    * Tests that findExecutor fails because Party is not AgreeToExecute request
    * */
    @Test
    @Tag("UnitTest")
    public void findExecutorMeatChannelFailsTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer);
        getMeatChannel(channelParticipants, meatChannelIngredients, transactionInformer);
        getMeatChannel().register(customer);
        Request request = new Request(customer, ONION, 1, customer.getOperationType());
        // Act
        Party executor = getMeatChannel().findExecutor(request);
        // Assert
        Assertions.assertNull(executor);
    }

}
