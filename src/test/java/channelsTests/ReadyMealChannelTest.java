package channelsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.channels.MeatChannel.getMeatChannel;
import static cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel.getReadyMealChannel;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;


public class ReadyMealChannelTest {
    TransactionInformer transactionInformer;
    Customer customer;
    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);
    Manufacturer manufacturer;
    List<Party> channelParticipants = new ArrayList<>();
    @BeforeEach
    public void init(){
        transactionInformer = new TransactionInformer();
        customer = new Customer("Kupec", 555, transactionInformer);
        manufacturer = new Manufacturer("Tomas", 5000, FoodEnum.BORSHCH, transactionInformer);
    }
    /*
     * Tests that TransactionInformer can't be null
     * */
    @Test
    @Tag("UnitTest")
    public void ReadyMeallChannelConstructorFails(){
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->getReadyMealChannel(channelParticipants,
                readyMeals,null));
    }
    /*
     * Ensures the Allowed entities for ReadyMealChannel are added correctly
     * */
    @Test
    @Tag("UnitTest")
    public void readyMealChannelAllowedEntitiesTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer, manufacturer);
        // Act
        getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        // Assert
        Assertions.assertEquals(getReadyMealChannel().getAllowedEntities(), readyMeals);
    }
    /*
     * Ensures that participants is registered to ReadyMealChannel correctly
     * */
    @Test
    @Tag("UnitTest")
    public void userRegisteredToReadyMealChannelTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer);
        // Act
        getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        // Assert
        getReadyMealChannel().register(customer);
        getReadyMealChannel().register(manufacturer);
        Assertions.assertTrue(getReadyMealChannel().isRegister(customer));
        Assertions.assertFalse(getReadyMealChannel().isRegister(manufacturer));
    }
    /*
     * Tests that findExecutor method performs correctly in ReadyMealChannel, manufacturer is ReadyToExecute
     * */
    @Test
    @Tag("UnitTest")
    public void findExecutorInReadyMealChannelTest(){
        // Arrange
        Collections.addAll(channelParticipants, manufacturer);
        getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        getReadyMealChannel().register(manufacturer);
        Request request  = new Request(manufacturer, BORSHCH, 1, manufacturer.getOperationType());
        // Act
        Party executor = getReadyMealChannel().findExecutor(request);
        // Assert
        Assertions.assertEquals(manufacturer, executor);
    }
    /*
     * Tests that findExecutor fails because Manufacturer has wrong specialization
     * */
    @Test
    @Tag("UnitTest")
    public void findExecutorFailedInReadyMealsTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer);
        getReadyMealChannel(channelParticipants, readyMeals, transactionInformer);
        getReadyMealChannel().register(customer);
        Request request = new Request(customer, DRANIKI, 1, customer.getOperationType());
        // Act
        Party executor = getReadyMealChannel().findExecutor(request);
        // Assert
        Assertions.assertNull(executor);
    }

}