package channelsTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.channels.MeatChannel.getMeatChannel;
import static cz.cvut.fel.omo.foodChain.channels.VegetableChannel.getVegetableChannel;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;


public class VegetableChannelTest {
    Customer customer;
    VegetableFarmer vegetableFarmer;
    TransactionInformer transactionInformer;

    List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    List<Party> channelParticipants = new ArrayList<>();

    @BeforeEach
    public void init(){
        transactionInformer = new TransactionInformer();
        customer = new Customer("Pokupatel", 555, transactionInformer);
        vegetableFarmer = new VegetableFarmer("Peter", "UK", 1000, 1990,
                vegetableIngredients, transactionInformer);
    }

    /*
     * Tests that TransactionInformer can't be null
     * */
    @Test
    @Tag("UnitTest")
    public void MeatChannelConstructorFails(){
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->getVegetableChannel(channelParticipants,
                vegetableIngredients,null));
    }
    /*
     * Ensures the Allowed entities are added correctly
     * */
    @Test
    @Tag("UnitTest")
    public void allowedEntitiesPerformsCorrectlyForVegetableChannelTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer, vegetableFarmer);
        // Act
        getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        // Assert
        Assertions.assertEquals(getVegetableChannel().getAllowedEntities(), vegetableIngredients);
    }
    /*
     * Ensures that participants is registered to channel correctly
     * */
    @Test
    @Tag("UnitTest")
    public void registrationForVegetableChannelPerformsCorrectlyTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer);
        // Act
        getVegetableChannel(channelParticipants,vegetableIngredients, transactionInformer);
        // Assert
        getVegetableChannel().register(customer);
        getVegetableChannel().register(vegetableFarmer);
        Assertions.assertTrue(getVegetableChannel().isRegister(customer));
        Assertions.assertFalse(getVegetableChannel().isRegister(vegetableFarmer));
    }
    /*
     * Tests that findExecutor method performs correctly
     * */
    @Test
    @Tag("UnitTest")
    public void findExecutorForVegetableChannelPerformsCorrectlyTest(){
        // Arrange
        Collections.addAll(channelParticipants, vegetableFarmer);
        getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        getVegetableChannel().register(vegetableFarmer);
        Request request  = new Request(vegetableFarmer, ONION, 1, vegetableFarmer.getOperationType());
        // Act
        Party executor = getVegetableChannel().findExecutor(request);
        // Assert
        Assertions.assertEquals(vegetableFarmer, executor);
    }
    /*
     * Tests that findExecutor fails because Party is not AgreeToExecute request
     * */
    @Test
    @Tag("UnitTest")
    public void findExecutorForVegetableChannelFailsTest(){
        // Arrange
        Collections.addAll(channelParticipants, customer);
        getVegetableChannel(channelParticipants, vegetableIngredients, transactionInformer);
        getVegetableChannel().register(customer);
        Request request = new Request(customer, ONION, 1, customer.getOperationType());
        // Act
        Party executor = getVegetableChannel().findExecutor(request);
        // Assert
        Assertions.assertNull(executor);
    }
}
