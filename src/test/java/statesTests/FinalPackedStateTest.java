package statesTests;

import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class FinalPackedStateTest {
    FoodEntity onion;
    FinalPackedState finalPackedState;
    @BeforeEach
    public void init(){
        onion = new FoodEntity(FoodEnum.ONION, 1, 5);
        finalPackedState = new FinalPackedState(onion);
    }
    /*
    * Ensures that Food is not possible to cook in Final Packed State
    * */
    @Test
    @Tag("UnitTest")
    public void UnableToCookTest(){
        // Act
        boolean actualCookState = finalPackedState.cook();
        // Assert
        Assertions.assertFalse(actualCookState);
    }
    /*
     * Ensures that Food can't be transported when packed
     * */
    @Test
    @Tag("UnitTest")
    public void TransportIsNotPossibleTest(){
        // Act
        boolean actualTransportState = finalPackedState.transport();
        // Assert
        Assertions.assertFalse(actualTransportState);
    }
    /*
     * Ensures that Food can always be presented to Customer when packed
     * */
    @Test
    @Tag("UnitTest")
    public void ProductAlwaysCanBePresentedTest(){
        // Act
        boolean presentProductState = finalPackedState.presentProductToCustomer();
        // Assert
        Assertions.assertTrue(presentProductState);
    }

}
