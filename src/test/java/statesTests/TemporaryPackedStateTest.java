package statesTests;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import org.junit.jupiter.api.*;

public class TemporaryPackedStateTest {
    FoodEntity butter;
    TemporaryPackedState temporaryPackedState;
    @BeforeEach
    public void init(){
        butter = new FoodEntity(FoodEnum.BUTTER, 1, -4);
        temporaryPackedState = new TemporaryPackedState(butter);
    }

    /*
     * Ensures that Food can't be cooked when Temporary Packed
     * */
    @Test
    @Tag("UnitTest")
    public void ImpossibleToCookTest(){
        // Act
        boolean actualCookState = temporaryPackedState.cook();
        // Assert
        Assertions.assertFalse(actualCookState);
    }

    /*
     * Ensures that Food can always be transported when Temporary Packed
     * */
    @Test
    @Tag("UnitTest")
    public void TransportIsAlwaysPossibleTest(){
        // Act
        boolean actualTransportState = temporaryPackedState.transport();
        // Assert
        Assertions.assertTrue(actualTransportState);
    }
    /*
     * Ensures that Food can't be presented to Customer
     * */
    @Test
    @Tag("UnitTest")
    public void ImpossibleToPresentToCustomerTest(){
        // Act
        boolean actualPresentState = temporaryPackedState.presentProductToCustomer();
        // Assert
        Assertions.assertFalse(actualPresentState);
    }
}
