package statesTests;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;
import org.junit.jupiter.api.*;

public class UnpackedStateTest {
    FoodEntity meat;
    UnpackedState unpackedStateTest;
    @BeforeEach
    public void init(){
        meat = new FoodEntity(FoodEnum.MEAT, 1, -12);
        unpackedStateTest = new UnpackedState(meat);
    }

    /*
    * Ensures that Food can always be cooked when Unpacked
    * */
    @Test
    @Tag("UnitTest")
    public void cookAlwaysTest(){
        // Act
        boolean actualCookState = unpackedStateTest.cook();
        // Assert
        Assertions.assertTrue(actualCookState);
    }

    /*
    * Ensures that transport operation is not available when Unpacked
    * */
    @Test
    @Tag("UnitTest")
    public void transportIsNotAvailableTest(){
        // Act
        boolean actualTransportState = unpackedStateTest.transport();
        // Assert
        Assertions.assertFalse(actualTransportState);
    }
    /*
    * Tests the present product is always impossible
    * */
    @Test
    @Tag("UnitTest")
    public void presentImpossibleTest(){
        // Act
        boolean actualUnpackedState = unpackedStateTest.presentProductToCustomer();
        // Assert
        Assertions.assertFalse(actualUnpackedState);
    }
}
