package visitorTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;

public class PriceWriterTest {
    static TransactionInformer transactionInformer = new TransactionInformer();
    static PriceWriter priceWriter = new PriceWriter();
    static List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    static List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    static List<FoodEnum> readyMeals = Arrays.asList(BORSHCH, PANCAKES, KYIVCUTLET, ICECREAM, DRANIKI);
    static List<FoodEnum> allProducts = Arrays.asList(BORSHCH, PANCAKES, KYIVCUTLET, ICECREAM, DRANIKI, POTATO, ONION,
            SUGAR, FLOUR, BEET, WATER, MILK, MEAT, BUTTER, EGGS);

    static VegetableFarmer vegetableFarmer = new VegetableFarmer("Mike", "USA", 150,
            1975, vegetableIngredients, transactionInformer);
    static MeatFarmer meatFarmer = new MeatFarmer("Michail", "Poland", 350,1990,
            meatIngredients, transactionInformer);
    static Manufacturer manufacturer = new Manufacturer("Rabotiaga", 1500, BORSHCH,
            transactionInformer);
    static Storehouse storehouse = new Storehouse("Podval", 1200, transactionInformer);
    static Retailer retailer = new Retailer("Mister", 4000, transactionInformer);
    static Seller seller = new Seller("SellerVasilij", 3500, transactionInformer);

    /*
    * Tests written prices for Vegetable Farmer
    * */
    @Test
    @Tag("UnitTest")
    public  void doForVegetableFarmerTest(){
        // Arrange
        List<Integer> actualPrices = new ArrayList<>();
        List<Integer> vegetableFarmerProductsExpectedPrices = Arrays.asList(18, 10, 20, 25, 15, 12);
        // Act
        priceWriter.doForVegetableFarmer(vegetableFarmer);
        for(FoodEnum ingredient: vegetableIngredients){
            actualPrices.add(vegetableFarmer.askPrice(ingredient, 1));
        }
        // Assert
        Assertions.assertEquals(vegetableFarmerProductsExpectedPrices, actualPrices);

    }

    /*
     * Tests written prices for Meat Farmer
     * */
    @Test
    @Tag("UnitTest")
    public  void doForMeatFarmerTest(){
        // Arrange
        List<Integer> actualPrices = new ArrayList<>();
        List<Integer> meatFarmerProductsExpectedPrices = Arrays.asList(20, 50, 25, 30);
        // Act
        priceWriter.doForMeatFarmer(meatFarmer);
        for(FoodEnum ingredient: meatIngredients){
            actualPrices.add(meatFarmer.askPrice(ingredient, 1));
        }
        // Assert
        Assertions.assertEquals(meatFarmerProductsExpectedPrices, actualPrices);
    }

    /*
     * Tests written prices for Manufacturer
     * */
    @Test
    @Tag("UnitTest")
    public  void doForManufacturerTest(){
        // Arrange
        List<Integer> actualPrices = new ArrayList<>();
        List<Integer> manufacturerProductsExpectedPrices = Arrays.asList(150, 160, 220, 100, 140);
        // Act
        priceWriter.doForManufacturer(manufacturer);
        for(FoodEnum ingredient: readyMeals){
            actualPrices.add(manufacturer.askPrice(ingredient, 1));
        }
        // Assert
        Assertions.assertEquals(manufacturerProductsExpectedPrices, actualPrices);
    }

    /*
     * Tests written prices for Storehouse
     * */
    @Test
    @Tag("UnitTest")
    public  void doForStorehouseTest(){
        // Arrange
        List<Integer> actualPrices = new ArrayList<>();
        List<Integer> storehouseProductsExpectedPrices =
                Arrays.asList(200, 220, 280, 120, 150, 24, 12, 28, 32, 18, 14, 25, 60, 30, 40);

        // Act
        priceWriter.doForStorehouse(storehouse);
        for(FoodEnum ingredient: allProducts){
            actualPrices.add(storehouse.askPrice(ingredient, 1));
        }
        // Assert
        Assertions.assertEquals(storehouseProductsExpectedPrices, actualPrices);
    }
    /*
     * Tests written prices for Retailer
     * */
    @Test
    @Tag("UnitTest")
    public  void doForRetailerTest(){
        // Arrange
        List<Integer> actualPrices = new ArrayList<>();
        List<Integer> retailerProductsExpectedPrices =
                Arrays.asList(250, 280, 320, 160, 180, 35, 25, 42, 48, 32, 22, 40, 73, 50, 55);
        // Act
        priceWriter.doForRetailer(retailer);
        for(FoodEnum ingredient: allProducts){
            actualPrices.add(retailer.askPrice(ingredient, 1));
        }
        // Assert
        Assertions.assertEquals(retailerProductsExpectedPrices, actualPrices);
    }

    /*
     * Tests written prices for Seller
     * */
    @Test
    @Tag("UnitTest")
    public  void doForSellerTest(){
        // Arrange
        List<Integer> actualPrices = new ArrayList<>();
        List<Integer> sellerProductsExpectedPrices =
                Arrays.asList(220, 240, 300, 140, 170, 30, 20, 34, 42, 25, 18, 35, 68, 40, 50);
        // Act
        priceWriter.doForSeller(seller);
        for(FoodEnum ingredient: allProducts){
            actualPrices.add(seller.askPrice(ingredient, 1));
        }
        // Assert
        Assertions.assertEquals(sellerProductsExpectedPrices, actualPrices);
    }




}
