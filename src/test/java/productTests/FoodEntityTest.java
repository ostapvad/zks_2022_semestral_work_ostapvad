package productTests;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.operations.Transaction;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;
import cz.cvut.fel.omo.foodChain.strategy.CookPancakes;
import dataConvertors.ExpectedOutcomeConvertor;
import dataConvertors.FoodEnumConvertor;
import dataConvertors.StringConvertor;
import dataConvertors.TransactionInformerConvertor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.List;

public class FoodEntityTest {

    /*
    * Mixed-strength test coverage for FoodEntity constructor
    * */
    @ParameterizedTest
    @Tag("ParametrizedTest")
    @CsvFileSource(resources = "/FoodEntityMixed.csv", numLinesToSkip = 1, delimiter = ',')
    public void MixedFoodEntityConstructorTest(@ConvertWith(FoodEnumConvertor.class)  FoodEnum name,
                                          int quantity, int storageTemperature,
                                          @ConvertWith(ExpectedOutcomeConvertor.class) boolean expectedOutcome){

        // Arrange
        boolean hasPassed = true;
        // Act
        try {new FoodEntity(name, quantity, storageTemperature);}
        catch (Exception e){
            hasPassed = false;}
        // Assert
        Assertions.assertEquals(expectedOutcome, hasPassed);

    }
    /*
    * Ensures that action is added to done actions correctly
    * */
    @Test
    @Tag("UnitTest")
    public void doneActionsPerformsCorrectlyTest(){
        // Arrange
        FoodEntity onion = new FoodEntity(FoodEnum.ONION, 1, 10);
        // Act
        onion.addAction("Cook");
        List<String> doneActions = onion.getDoneActions();
        // Assert
        Assertions.assertNotNull(doneActions);
        Assertions.assertEquals(1, doneActions.size() );
        Assertions.assertEquals("Cook", doneActions.get(0));


    }
    /*
    * Tests that doneActions() throws exception when new action is null
    * */
    @Test
    @Tag("UnitTest")
    public void doneActionsFailsTest(){
        // Arrange
        FoodEntity meat = new FoodEntity(FoodEnum.MEAT, 2, -5);
        // Act+Assert
        Assertions.assertThrows(IllegalArgumentException.class, ()->meat.addAction(null));
    }
}
