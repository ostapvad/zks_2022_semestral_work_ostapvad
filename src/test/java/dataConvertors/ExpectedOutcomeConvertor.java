package dataConvertors;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.util.ArrayList;
import java.util.List;

public class ExpectedOutcomeConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        return o.equals("Pass");
    }
}
