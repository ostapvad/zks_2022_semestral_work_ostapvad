package dataConvertors;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.util.ArrayList;
import java.util.List;

public class IngredientListConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("null")) {
            return null;
        }
        if(o.equals("[]")){
            return new ArrayList<FoodEnum>();
        }
        String ingredientsString = o.toString();
        String[] splittedIngredients = ingredientsString.substring(1, ingredientsString.length() - 1).split(";");
        List<FoodEnum> ingredients = new ArrayList<>();
        for (String ingredient: splittedIngredients){
            ingredients.add(FoodEnum.valueOf(ingredient));
        }
        return ingredients;
    }
}
