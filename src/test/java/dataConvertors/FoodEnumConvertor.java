package dataConvertors;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class FoodEnumConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("null")) {
            return null;
        }

        return  FoodEnum.valueOf(o.toString());

    }
}
