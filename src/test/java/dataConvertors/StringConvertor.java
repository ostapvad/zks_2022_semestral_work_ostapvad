package dataConvertors;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class StringConvertor extends SimpleArgumentConverter{
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("null")) {
            return null;
        }
        if (o.equals("''")){
            return "";
        }
        return o;
    }
}


