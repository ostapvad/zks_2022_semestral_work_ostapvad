package dataConvertors;

import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class TransactionInformerConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("null")) {
            return null;
        }
        return new TransactionInformer();
    }
}