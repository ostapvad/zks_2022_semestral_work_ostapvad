package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.visitor.Visitor;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.*;

public class Seller extends ChainParticipant {

    private List<FoodEntity> products = new ArrayList<>();
    private OperationEnum operationType = SELL;


    public Seller(String name, int money, TransactionInformer transactionInformer) {
        super(name, money, transactionInformer);
    }

    public void accept(Visitor visitor) {
        visitor.doForSeller(this);
    }

    public List<FoodEntity> getProducts() {
        return products;
    }

    /**
     * The method registers a chain participant in each of the channels.
     */
    public void registerToTheChannel() {
        MeatChannel.getMeatChannel().register(this);
        VegetableChannel.getVegetableChannel().register(this);
        ReadyMealChannel.getReadyMealChannel().register(this);
        PaymentChannel.getPaymentChannel().register(this);
    }

    @Override
    public OperationEnum getOperationType() {
        return operationType;
    }

    /**
     * Check if the seller can execute the request.
     *
     * @param request the instance of Request
     * @return true - if the farmer can execute the request
     */
    public boolean isAgreeToExecute(Request request) {
        for (FoodEntity foodEntity : products) {
            if (foodEntity.getName() == request.getFoodEnum()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sends the product to the applicant, removing it from its own list of products.
     *
     * @param request the request
     * @return the ingredient
     */
    public FoodEntity process(Request request) {
        for (FoodEntity foodEntity : products) {
            if (foodEntity.getName().equals(request.getFoodEnum())) {
                products.remove(foodEntity);
                request.done = true;
                foodEntity.transport(request.getApplicant());
                return foodEntity;
            }
        }
        logger.warning("Product wasn't taken from seller " + getName());
        return null;

    }

    /**
     * Add the entity of food to the list of products.
     *
     * @param foodEntity the instance of food that was produced by someone
     */
    public void addFood(FoodEntity foodEntity) {
        foodEntity.addAction(foodEntity + " with name " + foodEntity.getName() + " come to " + this);
        products.add(foodEntity);
    }
}
