package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;

public class Transaction {

    private Request request;
    private Party executor;
    private final int previousHash;
    private final int hash = hashCode();

    public Transaction(Request request, Party executor, int previousHash) {
        if(request == null) throw  new IllegalArgumentException("Transaction request can't be null");
        this.request = request;
        if(executor == null) throw new IllegalArgumentException("Transaction executor can't be null");
        this.executor = executor;
        if(previousHash < 0 ) throw  new IllegalArgumentException("Transaction previous hash can't be negative");
        this.previousHash = previousHash;
    }

    /**
     * uses to generate string for transactions report
     *
     * @return line for transactions report
     */
    public String getInfoForText() {
        return "TRANSACTION: " + "applicant = " + request.getApplicant() +
                ", executor = " + executor + ", foodEnum = " + request.getFoodEnum() +
                ", quantity = " + request.getQuantity() + ", operationType = " + request.getOperationType() +
                ", price = " + request.getPrice() + '.';
    }

    /**
     * uses to write info from transaction for transactions report
     */
    public void getTransactionInfo() {
        System.out.println(getInfoForText());
    }


    public int getHash() {
        return hash;
    }

    public int getPreviousHash() {
        return previousHash;
    }

    public OperationEnum getOperationType() {
        return request.getOperationType();
    }

    public String getSender() {
        return executor.getName();
    }

    public FoodEnum getProduct() {
        return request.getFoodEnum();
    }

    public int getQuantity() {
        return request.getQuantity();
    }
}
