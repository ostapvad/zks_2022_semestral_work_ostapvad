package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.operations.*;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.*;

/**
 * The class describes the general state and behavior of a farmer.
 *
 * @author Artem Hurbych, Pavel Paklonski
 */
public abstract class Farmer extends ChainParticipant {

    private String country;
    private List<FoodEnum> ingredientList;
    private List<FoodEntity> products = new ArrayList<>();
    private OperationEnum operationType = GROW;
    private int birthYear;

    public Farmer(String name, String country, int cashAccount, int birthYear, List<FoodEnum> ingredientList, TransactionInformer transactionInformer) {
        super(name, cashAccount, transactionInformer);
        // 1) Country ECs
        if(country == null) throw  new IllegalArgumentException("Country can't be null");
        if(country.length() < 2) throw  new IllegalArgumentException("The length of country can't be less than 2");
        if(country.length() > 46) throw  new IllegalArgumentException("The length of country can't be greater than 56");
        if(country.matches(".*\\d.*")) {
            throw new IllegalArgumentException("The country can't consist of numbers");
        }
        if(!Character.isUpperCase(country.charAt(0)))
            throw  new IllegalArgumentException("The first letter of country should be Capital");
        this.country = country;
        // 2) IngredientList ECs
        if(ingredientList == null) throw  new IllegalArgumentException("Ingredient list can't be null");
        if(ingredientList.isEmpty()) throw  new IllegalArgumentException("Ingredient list can't be empty");
        this.ingredientList = ingredientList;
        // 3) BirthYear ECs
        if(birthYear < 1920) throw new IllegalArgumentException("The birthYear can't be less than 1920");
        if(birthYear > 2023) throw new IllegalArgumentException("The birthYear can't be greater than 2023");
        this.birthYear = birthYear;

    }

    public OperationEnum getOperationType() {
        return operationType;
    }
    public int getBirthYear(){return birthYear;}

    /**
     * Check if the farmer can execute the request.
     *
     * @param request the instance of Request
     * @return true - if the farmer can execute the request
     */
    public boolean isAgreeToExecute(Request request) {
        return ingredientList.contains(request.getFoodEnum());
    }

    public abstract FoodEntity process(Request request);

    /**
     * Add the entity of food to the list of products.
     *
     * @param foodEntity the instance of food that was produced by someone
     */
    public void addFood(FoodEntity foodEntity) {
        products.add(foodEntity);
    }

}
