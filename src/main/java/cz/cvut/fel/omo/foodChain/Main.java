package cz.cvut.fel.omo.foodChain;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class Main {

    public final static Logger logger = Logger.getLogger("Loher");

    public static void main(String[] args) {}

    private static void generatePartiesReport(ChainParticipant chainParticipant){
        for(Iterator it = chainParticipant.getIterator(); it.hasNext(); ){
            Object text = it.next();
            if(text != null ) System.out.println(text);
        }
    }

    private static void generateTextPartiesReport(ChainParticipant chainParticipant){
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("PartyReport.txt"), "utf-8"))) {
            writer.write("-------------------- " + chainParticipant.getName() + " REPORT --------------------\n");
            for(Iterator it = chainParticipant.getIterator(); it.hasNext(); ){
                Object text = it.next();
                if(text != null) {
                    writer.write((String)text+ "\n");
                }
            }
        } catch (IOException e){
            logger.warning(e.getMessage());
        }
    }

}
