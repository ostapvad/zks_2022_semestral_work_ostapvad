package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;

public class Request {

    private Party applicant;
    private FoodEnum foodEnum;
    private int quantity;
    private OperationEnum operationType;
    private int price;
    public boolean done = false;


    public Request(Party applicant, FoodEnum foodEnum, int quantity, OperationEnum operationType) {
        if(applicant == null) throw new IllegalArgumentException("Request applicant can't be null");
        this.applicant = applicant;
        if(foodEnum == null) throw new IllegalArgumentException("Request foodEnum can't be null");
        this.foodEnum = foodEnum;
        if(quantity < 1) throw new IllegalArgumentException("Request Quantity can't be less than 1");
        if(quantity > 100)  throw new IllegalArgumentException("Request Quantity exceeds limit of 100");
        this.quantity = quantity;
        if(operationType == null)  throw new IllegalArgumentException("Request OperationType can't be null");
        this.operationType = operationType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Party getApplicant() {
        return applicant;
    }

    public FoodEnum getFoodEnum() {
        return foodEnum;
    }

    public int getQuantity() {
        return quantity;
    }

    public OperationEnum getOperationType() {
        return operationType;
    }

    @Override
    public String toString() {
        return "REQUEST: " + "applicant = " + applicant +
                ", foodEnum = " + foodEnum + ", quantity = " + quantity +
                ", operationType = " + operationType + '.';
    }
}
