package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.visitor.Visitor;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.operations.Request;

import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;

/**
 * The class describes the behavior of a farmer producing meat products.
 * @author Artem Hurbych, Pavel Paklonski
 */
public class MeatFarmer extends Farmer {

    public MeatFarmer(String name, String country, int cashAccount, int birthYear, List<FoodEnum> ingredientList, TransactionInformer transactionInformer) {
        super(name, country, cashAccount, birthYear, ingredientList, transactionInformer);
    }

    /**
     * The method registers a chain participant in each of the channels.
     */
    public void registerToTheChannel() {
        MeatChannel.getMeatChannel().register(this);
        PaymentChannel.getPaymentChannel().register(this);

    }


    public void accept(Visitor visitor) {
        visitor.doForMeatFarmer(this);
    }


    /**
     * Starts the product manufacturing process.
     * @param request the request
     * @return the ingredient
     */
    public FoodEntity process(Request request) {

        FoodEnum ingredientName = request.getFoodEnum();
        int quantity = request.getQuantity();
        FoodEntity ingredient = null;
        request.done = true;
        switch (ingredientName) {
            case MILK:
                ingredient = new FoodEntity(FoodEnum.MILK, quantity, 8);
                break;
            case MEAT:
                ingredient = new FoodEntity(FoodEnum.MEAT, quantity, 0);
                break;
            case BUTTER:
                ingredient = new FoodEntity(FoodEnum.BUTTER, quantity, -18);
                break;
            case EGGS:
                ingredient = new FoodEntity(FoodEnum.EGGS, quantity, 6);
                break;
        }
        if (ingredient == null) {
            logger.warning("Ingredient was not created by Farmer " + getName());
        } else {
            ingredient.addAction(ingredient + " with name " + ingredientName.toString() + " was created by " + this);
            ingredient.transport(request.getApplicant());
        }
        return ingredient;
    }
}